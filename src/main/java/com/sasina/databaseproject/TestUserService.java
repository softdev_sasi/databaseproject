/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sasina.databaseproject;

import com.sasina.databaseproject.model.User;
import com.sasina.databaseproject.service.UserService;

/**
 *
 * @author admin
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("user2", "password1");
        if(user != null){
            System.out.println("Welcome user: " + user.getName());
        } else {
            System.out.println("Error");
        }
    }
}
